import { Component } from "@angular/core";
import { Router } from "@angular/router";
import { INFO_DATA } from "./data-helper";

@Component({
  selector: "app-dashboard",
  templateUrl: "./dashboard.component.html",
  styleUrls: ["./dashboard.component.css"],
})
export class DashboardComponent {
  constructor(private router: Router) {}
  logout() {
    this.router.navigateByUrl("/");
  }
  medicalRecord() {
    this.router.navigateByUrl("/medical-record");
  }

  displayedColumns: string[] = ["position", "name", "phisician", "speciality"];
  dataSource = INFO_DATA;
  myColors = [
    {
      backgroundColor: "#e85752",
    },
    {
      backgroundColor: "rgba(103, 58, 183, .1)",
    },
  ];
  myColors2 = [
    {
      backgroundColor: "#a72879",
    },
    {
      backgroundColor: "rgba(222, 87, 33, .1)",
    },
  ];
  myColors3 = [
    {
      backgroundColor: "#e85752",
    },
    {
      backgroundColor: "#a72879",
    },
  ];
  chartOptions = {
    responsive: true,
  };
  chartData = [
    { data: [330], label: "Atendimentos do dia" },
    { data: [250], label: "Período anterior" },
  ];
  chartData2 = [
    { data: [30], label: "Atendimentos retorno" },
    { data: [200], label: "Período anterior" },
  ];
  chartData3 = [
    { data: [300], label: "Atendimentos do dia" },
    { data: [500], label: "Atendimentos cancelados" },
  ];
}
