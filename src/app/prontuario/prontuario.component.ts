import { Component } from "@angular/core";
import { Router } from "@angular/router";

@Component({
  selector: "prontuario-component",
  styleUrls: ["prontuario.component.css"],
  templateUrl: "./prontuario.component.html",
})
export class ProntuarioComponent {
  constructor(private router: Router) {}

  title: string = "Subjetiva";
  text: Array<string> = ["Subjetiva", "Objetiva", "Avaliação", "Plano"];
  count: number = 0;

  goFoward() {
    this.count = this.count + 1;
    this.count < 4 ? (this.title = this.text[this.count]) : (this.count = 3);
    return this.count;
  }
  goBackward() {
    this.count = this.count - 1;
    this.count >= 0 ? (this.title = this.text[this.count]) : (this.count = 0);
    return this.count;
  }

  logout() {
    this.router.navigateByUrl("/");
  }
  dashboard() {
    this.router.navigateByUrl("/dashboard");
  }
}
