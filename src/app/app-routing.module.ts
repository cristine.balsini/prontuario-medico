import { NgModule } from "@angular/core";
import { Routes, RouterModule } from "@angular/router";
import { HomeComponent } from "./home/home.component";
import { LoginPageComponent } from "./login-page/login-page.component";
import { DashboardComponent } from "./dashboard/dashboard.component";
import { ProntuarioComponent } from "./prontuario/prontuario.component";

const routes: Routes = [
  { path: "", component: HomeComponent },
  { path: "login", component: LoginPageComponent },
  { path: "dashboard", component: DashboardComponent },
  { path: "medical-record", component: ProntuarioComponent },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule],
})
export class AppRoutingModule {}
