import { Component } from "@angular/core";
import { FormControl, Validators } from "@angular/forms";
import { Router } from "@angular/router";

@Component({
  selector: "app-login-page",
  templateUrl: "./login-page.component.html",
  styleUrls: ["./login-page.component.css"],
})
export class LoginPageComponent {
  constructor(private router: Router) {}

  name = new FormControl("", [Validators.required, Validators.minLength(6)]);
  password = new FormControl("", [
    Validators.required,
    Validators.minLength(6),
  ]);

  onClick() {
    (this.name.value.length && this.password.value.length) < 6
      ? this.router.navigateByUrl("/login")
      : this.router.navigateByUrl("/dashboard");
  }

  exit() {
    this.router.navigateByUrl("/");
  }

  getErrorMessage() {
    if (this.name.hasError("required")) {
      return "Campo em branco";
    }

    return !this.name.hasError("minLength") ? "Mínimo 6 caracteres" : "";
  }

  getPasswordError() {
    if (this.password.hasError("required")) {
      return "Campo em branco";
    }

    return !this.password.hasError("minLength") ? "Mínimo 6 caracteres" : "";
  }
}
